require 'spec_helper'

describe Category do
  before { @cat = Category.new(name: "Mates") }

  subject { @cat }

  it { should respond_to(:name) }

  it { should be_valid }

  describe "cuando no tiene nombre" do
    before { @cat.name = " " }
    it { should_not be_valid } # no deberia ser valida una categoria sin nombre
  end

  describe "cuando el nombre es muy largo" do
    before { @cat.name = "a" * 51 }
    it { should_not be_valid } # no deberia ser tampoco valido
  end

  describe "cuando hay una categoria con ese nombre" do
    before do
      cat_with_same_name = @cat.dup
      cat_with_same_name.save
    end

    it { should_not be_valid } # no debe haber dos categorias con el mismo nombre
  end
end


  