require 'spec_helper'

describe "Static pages" do

  let(:base_title) { "#YoSoyEDU" } # asignamus un titulo base para los test

  describe "Pagina Inicio" do

    it "tiene que tener el contenido 'Bienvenidos'" do
      visit '/static_pages/home'
      expect(page).to have_content('Bienvenidos')
    end

    it "tiene que tener el titulo 'Inicio'" do
  		visit '/static_pages/home'
  		expect(page).to have_title("Inicio - #{base_title}")
	end
  end

  describe "Pagina de Ayuda" do

    it "tiene que tener el contenido 'Ayuda'" do
      visit '/static_pages/help'
      expect(page).to have_content('Ayuda')
    end

    it "tiene que tener el titulo 'Ayuda'" do
  		visit '/static_pages/help'
  		expect(page).to have_title("Ayuda - #{base_title}")
	end
  end

  describe "Pagina Sobre Nostros" do

    it "tiene que tener el contenido 'Sobre Nosotros'" do
      visit '/static_pages/about'
      expect(page).to have_content('Sobre Nosotros')
    end

    it "tiene que tener el titulo 'Sobre Nosotros'" do
  		visit '/static_pages/about'
  		expect(page).to have_title("Sobre Nosotros - #{base_title}")
	end
  end
end