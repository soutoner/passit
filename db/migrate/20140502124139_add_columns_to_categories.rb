class AddColumnsToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :s_desc, :string, default: 'Pulsa aquí para ver nuestros recursos'
    add_column :categories, :img, :string, default: 'cdefault.png'
  end
end
