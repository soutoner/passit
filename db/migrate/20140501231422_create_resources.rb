class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.belongs_to :category
      t.string :name
      t.string :url

      t.timestamps
    end
  end
end
