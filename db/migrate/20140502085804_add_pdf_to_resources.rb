class AddPdfToResources < ActiveRecord::Migration
  def change
    add_column :resources, :pdf, :boolean, default: false
  end
end
