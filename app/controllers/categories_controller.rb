class CategoriesController < ApplicationController
  before_filter :authenticate, :except => [:index, :show] # no pedir authenticate para show y index

  def new
  	@category = Category.new
  end

  def create
  	@category = Category.new(user_params)   
    if @category.save
      redirect_to @category
    else
      render 'new'
    end
  end

  def show
  	@category = Category.find(params[:id])
  end

  def index
    @categories = Category.all
  end

  protected
    def authenticate
      authenticate_or_request_with_http_basic do |username, password|
        username == 'admin' && password == 'admin' # simpel authentication
      end
    end

    def user_params
      params.require(:category).permit(:name)
    end

end
