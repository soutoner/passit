class ResourcesController < ApplicationController
	before_filter :authenticate, :except => [:show] # no pedir authenticate para show y index

  def new
    @resource = Resource.new
  end

  def create
    @resource = Resource.new(user_params)   
    if @resource.save
      redirect_to @resource
    else
      render 'new'
    end
  end

  def show
  	@resource = Resource.find(params[:id])
  end

  protected
    def authenticate
      authenticate_or_request_with_http_basic do |username, password|
        username == 'admin' && password == 'admin' # simple authentication
      end
    end

    def user_params
      params.require(:resource).permit(:name, :url, :category_id, :pdf)
    end

end
