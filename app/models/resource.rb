class Resource < ActiveRecord::Base
	validates :name, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
	validates :url, presence: true
	validates :category_id, presence: true
end
