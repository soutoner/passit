class Category < ActiveRecord::Base
	before_save { self.name = name.downcase } # name a minusculas para asegurar la singularidad de nombres
	validates :name, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
	has_many :resources
end