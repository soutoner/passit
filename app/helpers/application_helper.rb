module ApplicationHelper

	# De vuelve el título completo rellenando con el de cada pagina
  def full_title(page_title)
    base_title = "#YoSoyEDU"
    if page_title.empty? # si no hay tituo en la pagina solo se pone el base
      base_title
    else
      "#{page_title} - #{base_title}" # titulo de la pagina - hashtag
    end
  end

end
